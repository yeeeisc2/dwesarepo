import telepot
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardHide, ForceReply
from tinydb import TinyDB, Query
import json
import time
from datetime import datetime
import math

#default laguage
_default_language = "EN"

#replace the following string with the correct text
_rep_username    = '#username'#replace username
_rep_description = '#description'
_rep_date        = '#date'
_rep_km          = '#km'
_rep_carmins     = '#car_minutes'
_rep_walkmins    = '#walk_minutes'

#database's istances
db = TinyDB('json/db.json', default_table = 'users') # the db istance will not be used directly, instead is referred by the users and items table
_db_users = db.table('users') #users table
_db_items = db.table('items') #items table
_db_ni_pipe = db.table('new_items_pipe') #new_items buffer talbe

#database for messages logs
logsDB = TinyDB('json/logs.json')

#store the messages in json variable
tmpjsmx = open('./json/messages.json', 'r')
_json_messages = json.load(tmpjsmx)
tmpjsmx.close()

###TODO user obj and items obj with setter and getter
###TODO last action must be an array where [0] if the menu level (add_req, del_req, settings...) [1] is the secondo level, [2] is the third level....
#return all actions of the user
def usr_get_actions(chat_id):
	tmp = _db_users.search(Query().chat_id == chat_id)
	if tmp != []:
		tmp = tmp[0]['last_actions']
	return tmp

#update the last_action of the user
def usr_add_action(chat_id, action):
	tmp = usr_get_actions(chat_id)
	tmp.append(action)
	_db_users.update({'last_actions': tmp}, Query().chat_id == chat_id)

#increment the count of the items viewd
def usr_action_load_more(chat_id, add_items):
	tmp = usr_get_actions(chat_id)
	ac_lm = tmp.pop().split(",")
	date = ac_lm[1]
	n_items = ac_lm[2]
	#ac_lm = _btn_list_req + str(int(tmp.pop().replace(_btn_list_req, ""))+add_items)
	#tmp.append(_btn_list_req + str(int(tmp.pop().replace(_btn_list_req, ""))+add_items))
	#_db_users.update({'last_actions': tmp}, Query().chat_id == chat_id)

#creal all actions of the user
def usr_clear_action(chat_id):
	_db_users.update({'last_actions': []}, Query().chat_id == chat_id)

#remove the last action of the user
def usr_back_action(chat_id):
	tmp = usr_get_actions(chat_id)
	tmp.pop()
	_db_users.update({'last_actions': tmp}, Query().chat_id == chat_id)

#return user of the person
def usr_get_username(chat_id):
	tmpusr = _db_users.get(Query().chat_id == chat_id)
	if tmpusr != None:
		return tmpusr['username']
	else:
		#if error then return english like the default language
		print "eRRoR 560a - in usr_get_username("+str(chat_id)+"), user not found"
		return "--"

#return the language of the user
def usr_get_language(chat_id):
	tmpusr = _db_users.get(Query().chat_id == chat_id)
	if tmpusr != None:
		return tmpusr['language']
	else:
		#if error then return the default language
		print "eRRoR 560b - in usr_get_language("+str(chat_id)+"), user not found"
		return _default_language

#return the array of all languages that we have translate
def get_languages():
	return _json_messages['LANGUAGES_LIST']

#return the array of all languages that we have translate
def get_villages():
	villages = open('./json/villages_locations.json', 'r')
	data = json.load(villages)
	villages.close()
	return data


#array of errors during reading the messages in differente language
_error_reading_json_messages = {'EN':'ERROR while reading messages in different languages','IT':'ERRORE durante la lettura dei messaggi in lingue differenti','XO':''}

#return the message with language_id code in selected languages
 #return error message in _error_reading_languages if error given
def get_message(chat_id, messageID, _usr_language=None):
	if _usr_language == None:
		_usr_language = usr_get_language(chat_id)
	try:
		return _json_messages['messages'][messageID][_usr_language]
	except KeyError:
		#error during reading the messages in differente language
		print _error_reading_json_messages[_usr_language]
		print "--> " + messageID
		return _error_reading_json_messages[_usr_language]

#number of element to show each time
_load_more_n = 3
#return bidimensional array with
  #["keyboard"] is the keyboard to show
  #["msg"] is the message to send at the client
 #last_date is the last date of the last request that the user has seen
def load_more(chat_id, last_date=0):
	items_avaiable = _db_items.search((Query().available == 'true') & (Query().insertion_date > last_date))##errore se piu request sono state inserite nello stesso secondo

	if items_avaiable == []:
		return {"keyboard":"menu", "msg":get_message(chat_id, "2135")} #_db_items void

	else :
		##TODO sort items_avaiable from item.insertion_date
		#the date of the last request shown
		new_last_date = 0
		botAns = ""
		count = 0
		for el in items_avaiable :
			if count >= _load_more_n:
				#store the date of the latest item
				new_last_date = items_avaiable[count-1]['insertion_date']
				break
			botAns += get_message(chat_id, "item_card").replace(_rep_username, usr_get_username(el['chat_id'])).replace(_rep_description, el['description']).replace(_rep_date, datetime.fromtimestamp(int(el['insertion_date'])).strftime('%Y-%m-%d %H:%M'))
			count += 1
		botAns = get_message(chat_id, "2136") + botAns
		if new_last_date == 0:
			#no more requests to show
			botAns += get_message(chat_id, "2137")
			return {"keyboard":"menu", "msg": botAns}
		else:
			#update last action
			usr_clear_action(chat_id)
			usr_add_action(chat_id, _btn_list_req+","+str(new_last_date))
			#add how many request remain
			botAns += get_message(chat_id, "2138") +str(len(items_avaiable)-_load_more_n)+ get_message(chat_id, "2139")
			return {"keyboard":"list_req", "msg": botAns}

###TODO CREATE CLASS OF BUTTONS
#button variables
_btn_add_req           =  "BTN_ADD_REQUEST"
_btn_del_req           =  "BTN_DELETE_REQUEST"
_btn_list_req          =  "BTN_REQUEST_LIST"
_btn_load_more         =  "BTN_LOAD_MORE"
_btn_send_loc          =  "BTN_SEND_LOCATION"
_btn_skip              =  "BTN_SKIP_STEP"
_btn_cancel            =  'BTN_CANCEL'
_btn_settings          =  "BTN_SETTINGS"
_btn_help              =  "BTN_HELP"
_btn_language          =  "BTN_LANGUAGE"
_btn_villages          =  "BTN_VILLAGES"
_btn_enable_broadcast  =  "BTN_ENABLE_BROADCAST"
_btn_disable_broadcast =  "BTN_DISABLE_BROADCAST"
_btn_back              =  "BTN_BACK"

#return the text of the button in correct language
def get_button(chat_id, btn_code):
	_usr_language = usr_get_language(chat_id)
	try:
		return _json_messages['buttons'][btn_code][_usr_language]
	except KeyError:
		#error during reading the messages in differente language
		print "error get_button"
		print "--> " + btn_code
		return ""

##TODO CLASS of keyboards
# kb is the name of keyboard the you need
# chat_id of the current user (required only in settingsKeyboard)
def get_keyboard(chat_id, kb):

	if kb == "done":
		return {'keyboard': [[KeyboardButton(text="DONE")]] }

	elif kb == "menu":
		usr_clear_action(chat_id)
		return {'keyboard': [[get_button(chat_id, _btn_add_req)], [get_button(chat_id, _btn_list_req)], [get_button(chat_id, _btn_del_req)], [get_button(chat_id, _btn_settings)]]}

	elif kb == "position":
		keyboardButtons=[]
		keyboardButtons.append([KeyboardButton(text=get_button(chat_id, _btn_send_loc), request_location=True)])
		keyboardButtons.append([KeyboardButton(text=get_button(chat_id, _btn_villages))])
		keyboardButtons.append([KeyboardButton(text=get_button(chat_id, _btn_cancel))])
		return {'keyboard': keyboardButtons }

	elif kb == "villages":
		keyboardButtons=[[KeyboardButton(text=get_button(chat_id, _btn_cancel))], [KeyboardButton(text=get_button(chat_id, _btn_back))]]
		data = get_villages()
		for req in data :
			keyboardButtons.append([KeyboardButton(text=req)])
		return {'keyboard': keyboardButtons }

	elif kb == "settings": #chat_id must be given
		#check if the broadcast is already enable or disable
		brd_flg = _db_users.search(Query().chat_id == chat_id)
		if brd_flg[0]['broadcast'] == "true":
			set_broadcast = _btn_disable_broadcast
		else:
			set_broadcast = _btn_enable_broadcast
		return {'keyboard': [[get_button(chat_id, _btn_language)],[get_button(chat_id, set_broadcast)], [get_button(chat_id, _btn_help)], [get_button(chat_id, _btn_back)]]}

	elif kb == "languages":
		#create the languages keyboard
		languagesKeyboard = []
		#first item is cancel
		languagesKeyboard.append([get_button(chat_id, _btn_cancel)])
		#add all languages
		for language in get_languages():
			languagesKeyboard.append([language[0]])
		#return
		return {'keyboard':languagesKeyboard}

	elif kb == "add_req":
		return {'keyboard': [[get_button(chat_id, _btn_cancel)]]}

	elif kb == "list_req":
		return {'keyboard': [[get_button(chat_id, _btn_load_more)], [get_button(chat_id, _btn_back)]]}

	elif kb == "del_req":
			keyboardButtons=[]
			keyboardButtons.append([KeyboardButton(text=get_button(chat_id, _btn_cancel))])
			deleteKeyboard = None
			data = _db_items.search(Query().chat_id == chat_id)
			flag = False
			for req in data :
				if req['available'] == 'true':
					keyboardButtons.append([KeyboardButton(text=req['description'])])
					flag = True
			if flag:
				#if there are elements to be deleted return a keyboard else it will return []
				deleteKeyboard = {'keyboard': keyboardButtons }
			return deleteKeyboard

	elif kb == "hide":
		return ReplyKeyboardHide()

	else:
		print "ERROR in get_keyboard 12984912"
		return ReplyKeyboardHide()

#send item with item_eid in broadcast (exept sender)
def share_item(item_eid, sender_id, sender_name):
	#clear my pipe
	#give_me_my_pipe(sender_id)

	item = _db_items.get(eid=item_eid)
	usr_brdcast_on = _db_users.search((Query().chat_id != sender_id) & (Query().broadcast == 'true'))
	print "added " + item["description"] ##DEBUG
	
	for t_user in usr_brdcast_on :
		chat_id = t_user['chat_id']
		usr_actions = usr_get_actions(chat_id)

		bot.sendMessage(chat_id, get_message(chat_id, "new_item_card").replace(_rep_username, sender_name).replace(_rep_description, item["description"]))
		
		#don't send message if the user is adding a request
		'''if (len(usr_actions) == 0) or (usr_actions[0] != _btn_add_req):
			bot.sendMessage(chat_id, get_message(chat_id, "new_item_card").replace(_rep_username, sender_name).replace(_rep_description, item["description"]))
		
		#then add the message into pipe
		else:
			_db_ni_pipe.insert_multiple([{'recipient_id': chat_id,
										  'sender_id': sender_id,
										  'item_eid': item_eid,
										  'insertion_date': int(time.time())}])'''

#send all items in the pipe at the user that has just finished the insertion of request
def give_me_my_pipe(chat_id):
	user_pipe = _db_ni_pipe.search(Query().recipient_id == chat_id)
	for item_pipe in user_pipe:
		if (int(time.time()) - 60*5) < item_pipe['insertion_date']:
			#send item
			##TODO controlla che l'utente sia ancora nel db
			sender_name = usr_get_username(item_pipe["sender_id"])
			##TODO controlla che l'item non sia stato eliminato
			item = _db_items.get(eid=item_pipe.eid)
			bot.sendMessage(chat_id, get_message(chat_id, "new_item_card").replace(_rep_username, sender_name).replace(_rep_description, item["description"]))			

		print item_pipe.eid
		print json.dumps(item_pipe, indent=2)
		#delete item
		_db_ni_pipe.remove(eids=[item_pipe.eid])

#bot already started, than clear users last action and send the welcome back message
def start():
	all_users = _db_users.all()
	for req in all_users :
		#bot.sendMessage(req['chat_id'], get_message(req['chat_id'],'2116'), reply_markup=get_keyboard(req['chat_id'], "menu"))
		usr_clear_action(req['chat_id'])

#insert the message log in logsDB
def save_log(msg):
	message = {'message': msg }
	logsDB.insert_multiple([message])

#get the nearest village given a position
def nearest_village(loc):
	#set the distance to a extremely high value in order to always get the nearest village 
	nearest_village =['',1000000000]
	#retrieve villages from json
	data = get_villages()
	for req in data :
		tmp_distance = distance_on_unit_sphere(
			loc['latitude'],
			loc['longitude'],
			data[req]['latitude'],
			data[req]['longitude'])
		if tmp_distance < nearest_village[1]:
			nearest_village = (req,tmp_distance)
	#return string rapresentation of the village
	return nearest_village[0]



#get the distance on the earth surface (ignoring geologic irregularities)
def distance_on_unit_sphere(lat1, long1, lat2, long2):

	# Convert latitude and longitude to
	# spherical coordinates in radians.
	degrees_to_radians = math.pi/180.0

	# phi = 90 - latitude
	phi1 = (90.0 - lat1)*degrees_to_radians
	phi2 = (90.0 - lat2)*degrees_to_radians

	# theta = longitude
	theta1 = long1*degrees_to_radians
	theta2 = long2*degrees_to_radians

	# Compute spherical distance from spherical coordinates.

	# For two locations in spherical coordinates
	# (1, theta, phi) and (1, theta', phi')
	# cosine( arc length ) =
	# sin phi sin phi' cos(theta-theta') + cos phi cos phi'
	# distance = rho * arc length

	cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
	math.cos(phi1)*math.cos(phi2))
	arc = math.acos( cos )

	# Remember to multiply arc by the radius of the earth
	# in your favorite set of units to get length.
	return arc


#main function
def handle(msg):

	#save the message to the logsDB
	save_log(msg)

	#variables from the message
	user_message = msg['text'] if "text" in msg else None
	user_location = msg["location"] if "location" in msg else None
	#user data
	 ##TODO create user class
	chat_id = msg['chat']['id']
	username = msg['from']['username'] if 'username' in msg['from'] else None
	first_name = msg['from']['first_name'] if 'first_name' in msg['from'] else None
	last_name = msg['from']['last_name'] if 'last_name' in msg['from'] else None

	#print json.dumps(msg, indent=2)

	if username == None :
		bot.sendMessage(chat_id, get_message(chat_id, "2128", _default_language), reply_markup=get_keyboard(chat_id, "done"))
	
	else:

		#update the username of the chat_id if changed
		if 0 != len(_db_users.search(Query().chat_id == chat_id)):
			if usr_get_username(chat_id) != username:
				_db_users.update({'username': username}, Query().chat_id == chat_id)

		#else
		usr_actions = usr_get_actions(chat_id)
        
##livello 0 - main menu
		if   len(usr_actions) == 0:

			#first login
			if 0 == len(_db_users.search(Query().chat_id == chat_id)):
				#store the user if this is his first time
				_db_users.insert({'chat_id':chat_id,
								  'username':username,
								  'first_name':first_name,
								  'last_name':last_name,
								  'first_date':int(time.time()),
								  'last_actions':[],
								  'broadcast':'true',
								  'language':'EN'
								  })
				bot.sendMessage(chat_id, get_message(chat_id, "welcome").replace(_rep_username, username))
				bot.sendMessage(chat_id, get_message(chat_id, "2103"), reply_markup=get_keyboard(chat_id, "menu"))

			elif "/help" == user_message :
				bot.sendMessage(chat_id, get_message(chat_id, "2103"), reply_markup=get_keyboard(chat_id, "menu"))

			elif  user_message == get_button(chat_id, _btn_add_req):
				bot.sendMessage(chat_id, get_message(chat_id, "2105"), reply_markup=get_keyboard(chat_id, "add_req"))
				usr_add_action(chat_id, _btn_add_req)

			elif  user_message == get_button(chat_id, _btn_list_req):
				lm = load_more(chat_id)
				bot.sendMessage(chat_id, lm["msg"], reply_markup=get_keyboard(chat_id, lm["keyboard"]))

			elif  user_message == get_button(chat_id, _btn_del_req):
				deleteKeyboard = get_keyboard(chat_id, "del_req")
				if deleteKeyboard!=None:
					bot.sendMessage(chat_id, get_message(chat_id, "2117"), reply_markup=get_keyboard(chat_id, "del_req"))
					usr_add_action(chat_id, _btn_del_req)
				else:
					bot.sendMessage(chat_id, get_message(chat_id, "2130"), reply_markup=get_keyboard(chat_id, "menu"))

			elif  user_message == get_button(chat_id, _btn_settings):
				bot.sendMessage(chat_id, get_message(chat_id, "2125"), reply_markup=get_keyboard(chat_id, "settings"))
				usr_add_action(chat_id, _btn_settings)
			else:
				bot.sendMessage(chat_id, get_message(chat_id, "2127"), reply_markup=get_keyboard(chat_id, "menu"))

#livello 1
		elif len(usr_actions) == 1:
	#add request
			if usr_actions[0] == _btn_add_req:

				if  user_message == get_button(chat_id, _btn_cancel):
					bot.sendMessage(chat_id, get_message(chat_id, "2124"), reply_markup=get_keyboard(chat_id, "menu"))

				elif user_message != None:
					entryInserted = _db_items.insert_multiple([{'chat_id':chat_id,
																'description':user_message,
																'position':{'latitude' : 0, 'longitude' : 0},
																'nearest_village': '',
																'available':'true',
																'insertion_date':int(time.time())
																}])
					bot.sendMessage(chat_id, get_message(chat_id, "2106"), reply_markup=get_keyboard(chat_id, "position"))
					usr_add_action(chat_id, int(str(entryInserted).replace('[', '').replace(']', '')))

				else :
					bot.sendMessage(chat_id, get_message(chat_id, "2112"), reply_markup=get_keyboard(chat_id, "add_req"))
                                    
	#delete request
			elif usr_actions[0] == _btn_del_req:

				if  user_message == get_button(chat_id, _btn_cancel):
					bot.sendMessage(chat_id, get_message(chat_id, "2111"), reply_markup=get_keyboard(chat_id, "menu"))

				else:

					#search the item
					data = _db_items.search(Query().chat_id == chat_id)
					reply =''
					for req in data :
						if req['available'] == 'true' and req['description'] == user_message :
							reply = req['description']

					if reply == '':
						bot.sendMessage(chat_id, get_message(chat_id, "2129"), reply_markup=get_keyboard(chat_id, "del_req"))
					else:
						_db_items.update({'available': 'false'}, Query().chat_id == chat_id and Query().description == reply)
						bot.sendMessage(chat_id, get_message(chat_id, "2107"), reply_markup=get_keyboard(chat_id, "menu"))
	#request list
			elif _btn_list_req in usr_actions[0]:

				if user_message == get_button(chat_id, _btn_back):
					bot.sendMessage(chat_id, get_message(chat_id, "2125"), reply_markup=get_keyboard(chat_id, "menu"))

				elif user_message == get_button(chat_id, _btn_load_more):
					lm = load_more(chat_id, int(usr_actions[0].split(",")[1]))
					bot.sendMessage(chat_id, lm["msg"], reply_markup=get_keyboard(chat_id, lm["keyboard"]))

				else:
					bot.sendMessage(chat_id, get_message(chat_id, "2120"), reply_markup=get_keyboard(chat_id, "list_req"))
	#settings
			elif usr_actions[0] == _btn_settings:

				if  user_message == get_button(chat_id, _btn_language):
					bot.sendMessage(chat_id, get_message(chat_id, "2126"), reply_markup=get_keyboard(chat_id, "languages"))
					usr_add_action(chat_id, _btn_language)

				elif  user_message == get_button(chat_id, _btn_disable_broadcast):
					#update the user broadcast flag
					_db_users.update({"broadcast": "false"}, Query().chat_id == chat_id)
					#send the message and the correct keyboard
					bot.sendMessage(chat_id, get_message(chat_id, "2121"), reply_markup=get_keyboard(chat_id, "menu"))

				elif  user_message == get_button(chat_id, _btn_enable_broadcast):
					#update the user broadcast flag
					_db_users.update({"broadcast": "true"}, Query().chat_id == chat_id)
					#send the message and the correct keyboard
					bot.sendMessage(chat_id, get_message(chat_id, "2122"), reply_markup=get_keyboard(chat_id, "menu"))

				elif user_message == get_button(chat_id, _btn_help):
					bot.sendMessage(chat_id, get_message(chat_id, "2103"), reply_markup=get_keyboard(chat_id, "menu"))

				elif user_message == get_button(chat_id, _btn_back):
					bot.sendMessage(chat_id, get_message(chat_id, "2125"), reply_markup=get_keyboard(chat_id, "menu"))

				else:
					bot.sendMessage(chat_id, get_message(chat_id, "2120"), reply_markup=get_keyboard(chat_id, "settings"))

#livello 2
		elif len(usr_actions) == 2:

	#settings
			if usr_actions[0] == _btn_settings:

		#language
				if usr_actions[1] == _btn_language:

					if  user_message == get_button(chat_id, _btn_cancel):
						bot.sendMessage(chat_id, get_message(chat_id, "2133"), reply_markup=get_keyboard(chat_id, "menu"))

					else:
						flag = False
						for language in get_languages():
							if language[0] == user_message:
								flag = True
								_db_users.update({"language": language[1]}, Query().chat_id == chat_id)
								bot.sendMessage(chat_id, get_message(chat_id, "2123"), reply_markup=get_keyboard(chat_id, "menu"))
								break

						if not flag:
							bot.sendMessage(chat_id, get_message(chat_id, "2134"), reply_markup=get_keyboard(chat_id, "languages"))
	
#add request
			if usr_actions[0] == _btn_add_req:

				eid = usr_actions[1]

				if  user_message == get_button(chat_id, _btn_cancel):
					_db_items.remove(eids=[eid])
					bot.sendMessage(chat_id, get_message(chat_id, "2124"), reply_markup=get_keyboard(chat_id, "menu"))

				elif user_message == get_button(chat_id, _btn_villages):
					bot.sendMessage(chat_id, get_message(chat_id, "2131"), reply_markup=get_keyboard(chat_id, "villages"))
					usr_add_action(chat_id, _btn_villages)

				elif user_location != None:
					_db_items.update({'nearest_village': nearest_village(user_location)}, eids=[eid])
					_db_items.update({'position': {'latitude' : user_location['latitude'], 'longitude' : user_location['longitude']}}, eids=[eid])
					bot.sendMessage(chat_id, get_message(chat_id, "2108"), reply_markup=get_keyboard(chat_id, "menu"))
					share_item(eid, chat_id, username)

				else:
					bot.sendMessage(chat_id, get_message(chat_id, "2132"), reply_markup=get_keyboard(chat_id, "position"))

#livello 3
		elif len(usr_actions) == 3:

	#add request
			if usr_actions[0] == _btn_add_req:

				eid = usr_actions[1]

				if usr_actions[2] == _btn_villages:

					if  user_message == get_button(chat_id, _btn_cancel):
						_db_items.remove(eids=[eid])
						bot.sendMessage(chat_id, get_message(chat_id, "2124"), reply_markup=get_keyboard(chat_id, "menu"))

					elif user_message == get_button(chat_id, _btn_back):
						bot.sendMessage(chat_id, get_message(chat_id, "2132"), reply_markup=get_keyboard(chat_id, "position"))
						usr_back_action(chat_id)

					else:
						flag = False
						data = get_villages()
						for req in data :
							if req == user_message :
								flag = True
								_db_items.update({'position': {'latitude' : data[req]['latitude'], 'longitude' : data[req]['longitude']}}, eids=[eid])
								bot.sendMessage(chat_id, get_message(chat_id, "2108"), reply_markup=get_keyboard(chat_id, "menu"))
								share_item(eid, chat_id, username)
								break

						if not flag:
							bot.sendMessage(chat_id, get_message(chat_id, "2131"), reply_markup=get_keyboard(chat_id, "villages"))


#_db_users.purge()
#_db_items.purge()
#_db_ni_pipe.purge()

#return the token from json file
def getToken(username):
	tmptoken  = open('./json/tokenbot.json', 'r')
	tmptokenn = json.load(tmptoken)
	tmptoken.close()
	return str(tmptokenn[username])

#create bot
bot = telepot.Bot(getToken("token"))
print('[+] started...')
#send the welcome back message
start()
#start listening
bot.message_loop(handle, run_forever=True)