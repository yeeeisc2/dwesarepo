from tinydb import TinyDB, Query
import json
import time

db = TinyDB('json/test_dbinfo.json', default_table = 'users')

users = db.table('users')
items = db.table('items')
ni_pipe = db.table('new_items_pipe')


#insert new user
insert = {}
#insert = {'chat_id':'', 'username':'', 'first_name':'', 'last_name':'', 'first_date':0, 'last_action':'', 'broadcast':'true', 'language':'EN'}
insert['chat_id'] = '163076494'
insert['username'] = 'Magikone'
insert['first_name'] = 'Luca'
insert['last_name'] = 'Galasso'
insert['first_date'] = int(time.time())
insert['last_actions'] = []
insert['broadcast'] = 'true'
insert['language'] = 'EN'
users.insert_multiple([insert])
insert.clear()

#insert = {'chat_id':'', 'description':'', 'position':{'latitude' : 0, 'longitude' : 0}, 'available':'true', 'insertion_date':0}
insert['chat_id'] = '163076494'
insert['description'] = 'Bastone'
insert['position'] = {'latitude' : 0, 'longitude' : 0}
insert['available'] = 'true'
insert['insertion_date'] = int(time.time())
items.insert_multiple([insert])
insert.clear()

#insert = {'recipient_id':'', 'item_eid':'', 'insertion_date':''}
insert['recipient_id'] = '163076494'
insert['item_eid'] = '1'
insert['insertion_date'] = int(time.time())
ni_pipe.insert_multiple([insert])
insert.clear()

#data = users.search((Query().username == 'Magikone'))


print json.dumps(users.all(), indent=2)
print json.dumps(items.all(), indent=2)



#users.purge()
#items.purge()
print



