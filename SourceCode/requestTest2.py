import telepot
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardHide, ForceReply
from tinydb import TinyDB, Query
import json
import time
import datetime

#selected language
__language = 'EN'
#commands variables
__str_request = 'ADD REQUEST'
__str_delete  = 'DELETE YOUR REQUEST'
__str_all     = 'REQUEST LIST'
__str_pos     = 'SEND POSITION'
__str_skip     = 'SKIP STEP'
__str_canc     = 'CANCEL'
__str_deciding = 'DECIDING'
__str_purge   = '/purge'
#buttons variables
__btn_addRequest = 'ADD REQUEST'
__btn_requestList = 'REQUEST LIST'
__error_reading_languages = {'EN':'ERROR reading messages in different languages','IT':'ERRORE durante la lettura dei messaggi in lingue differenti','XO':''}
#put the following variables in the correct file
__str_username = '#nomeutente'
#TBD : ALL the previous variables are to be added in another python file

#database's istances
db = TinyDB('db.json', default_table = 'users') # the db istance will not be used directly, instead is referred by the users and items table
users = db.table('users') #users table
items = db.table('items') #items table

#read all messages languages
#read the file
jsonFile = open('./json/languages.json', 'r')
#store the file in json variable
languages = json.load(jsonFile)
jsonFile.close()

#function that read the message in differente languages
def select_message(language_id):
    #check if 'languages' exist in languages
    if 'languages' in languages:
        #check if language_id exist in languages['languages']
        if language_id in languages['languages']:
            if __language in languages['languages'][language_id]:
                #all correct return
                return languages['languages'][language_id][__language]
            print 'translation not found'
        print 'id not found'
    print __error_reading_languages[__language]
    #error return
    return __error_reading_languages[__language]

def handle(msg):
    #variables from the message
    command = msg['text'] if "text" in msg else None
    #user data
    chat_id = msg['chat']['id']
    username = msg['from']['username'] if 'username' in msg['from'] else None
    first_name = msg['from']['first_name'] if 'first_name' in msg['from'] else None
    last_name = msg['from']['last_name'] if 'last_name' in msg['from'] else None
    #keyboards
    mainMenuKeyboard = {'keyboard': [['ADD REQUEST','REQUEST LIST'], ['DELETE YOUR REQUEST','SETTINGS']]}
    positionKeyboard = {'keyboard': [[KeyboardButton(text='SEND POSITION', request_location=True),'SKIP STEP'], ['CANCEL']]}
    
    #store the user if is not in the database
    if 0 == len(users.search(Query().chat_id == chat_id)):
        #store the user if this is his first time
        users.insert({'chat_id':chat_id, 'username':username, 'first_name':first_name, 'last_name':last_name, 'first_date':int(time.time()), 'last_date':int(time.time()), 'last_action':0})

    #print json.dumps(msg, indent=2)

    #check if the command is valid
    if command!=None:

        if '/start' in command :
            #bot.sendMessage(chat_id,select_message('2102').replace(__str_username, username))
            bot.sendMessage(chat_id,select_message('2103'),reply_markup=mainMenuKeyboard)
            #TODO send keyboard buttons

        elif command == __str_request:
            #bot.sendMessage(chat_id, select_message('2104'))
            markup = ReplyKeyboardHide()
            bot.sendMessage(chat_id, select_message('2105'), reply_markup=markup)
            #update the action that the user chosen
            users.update({'last_action': __str_request}, Query().chat_id == chat_id)

        elif command == __str_delete:
            data = items.search(Query().chat_id == chat_id)
            #show the list with the request
            keyboardButtons=[]
            for req in data :
                if req['available'] == 'true':
                	keyboardButtons.append([KeyboardButton(text=req['description'])])
            keyboardButtons.append([KeyboardButton(text=__str_canc)])
            if keyboardButtons!=[[KeyboardButton(text=__str_canc, request_contact=None, request_location=None)]] :
	            deleteKeyboard = {'keyboard': keyboardButtons }
	            bot.sendMessage(chat_id, 'Please select the request that you want to delete:\n', reply_markup=deleteKeyboard)
	            #set the latest user action
	            users.update({'last_action': __str_delete}, Query().chat_id == chat_id)
            else:
	        	bot.sendMessage(chat_id,"You haven't submitted any request yet!",reply_markup=mainMenuKeyboard)

        elif command == __str_all:
            botAns = ''
            allData = items.all()
            print allData
            if allData!= [] :
	            for el in allData :
	            	if el['available'] == 'true':
	                	botAns += el['description'] + ' at ' +  datetime.datetime.fromtimestamp(int(el['date'])).strftime('%Y-%m-%d %H:%M:%S') + '\n'
	            bot.sendMessage(chat_id, botAns)
	            users.update({'last_action': __str_all}, Query().chat_id == chat_id)
            else :
            	bot.sendMessage(chat_id,select_message('No requests at the moment!'),reply_markup=mainMenuKeyboard)


        elif command == __str_purge:
            users.purge()
            items.purge()
            bot.sendMessage(chat_id, 'Purged')

        else :
			#get last action of the user
			action = users.search(Query().chat_id == chat_id)
			action = action[0]['last_action']
			if action == __str_request:
				#store the item
				entryInserted = items.insert_multiple([{'chat_id':chat_id, 'description':command, 'position':{'latitude' : 0, 'longitude' : 0}, 'available':'true', 'date':int(time.time())}])
				bot.sendMessage(chat_id, select_message('2106'))
				#bot.sendMessage(chat_id, select_message('2107'))
				users.update({'last_action': (__str_deciding+str(entryInserted))}, Query().chat_id == chat_id)
				bot.sendMessage(chat_id, 'Thanks! Now would you send me your position please?', reply_markup=positionKeyboard)
			elif __str_deciding in action :
				if command == __str_skip:
					users.update({'last_action': __str_skip}, Query().chat_id == chat_id)
				elif command == __str_canc:
					eid = action
					eid = eid.replace(__str_deciding, "")
					eid = eid.replace('[', "")
					eid = eid.replace(']', "")
					items.remove(eids=[int(eid)])
					users.update({'last_action': __str_canc}, Query().chat_id == chat_id)
				bot.sendMessage(chat_id,select_message('2103'),reply_markup=mainMenuKeyboard)
			elif action == __str_delete:
				if __str_canc == command :
					bot.sendMessage(chat_id,select_message('2103'),reply_markup=mainMenuKeyboard)
				else :
					#search the item
					data = items.search(Query().chat_id == chat_id)
					reply =''
					for req in data :
						if req['available'] == 'true':
							if req['description'] == command :
								reply = req['description']
					items.update({'available': 'false'}, Query().chat_id == chat_id and Query().description == reply)
					bot.sendMessage(chat_id, 'Your request has been deleted from the system', reply_markup=mainMenuKeyboard)
			else :
				bot.sendMessage(chat_id, 'Please select an option first')
				users.update({'last_action': 0}, Query().chat_id == chat_id)
    else :
    	action = users.search(Query().chat_id == chat_id)
    	action = action[0]['last_action']
    	text_location = msg["location"] if "location" in msg else None 
    	if __str_deciding in action and text_location != None :
    		eid = action
    		eid = eid.replace(__str_deciding, "")
    		eid = eid.replace('[', "")
    		eid = eid.replace(']', "")
    		print text_location['latitude'], text_location['longitude']
    		users.update({'last_action': __str_pos}, Query().chat_id == chat_id)
    		items.update({'position': {'latitude' : text_location['latitude'], 'longitude' : text_location['longitude']}}, eids=[int(eid)])
    		bot.sendMessage(chat_id, 'Thanks! Your request has been added to the system', reply_markup=mainMenuKeyboard)




bot = telepot.Bot('token')
print('[+] started...')
#bot.sendMessage(144892352, 'Bella silvio')
bot.message_loop(handle, run_forever=True)


