from tinydb import TinyDB, Query


#database's istances
db = TinyDB('json/db.json', default_table = 'users') # the db istance will not be used directly, instead is referred by the users and items table
_db_users = db.table('users') #users table
_db_items = db.table('items') #items table

_db_users.purge()
_db_items.purge()