import telepot
import string

main_keyboard_EN = [['1. ADD JOURNEY'], ['2. SEARCH JOURNEY'], ['3. REPORT'], ['4. CHANGE LANGUAGE']]
main_keyboard_XO = [['1: UTHUTHO'], ['2: SEARCH OBELUSINGISE'], ['3: INGXELO'], ['4: TSHINTSHA ULWIMI']]
add_date_EN = [['1.1 2016-09-14'], ['1.1 2016-09-15'], ['1.1 2016-09-16'], ['_. Main menu']]
add_date_XO = [['1:1 2016-09-14'], ['1:1 2016-09-15'], ['1:1 2016-09-16'], ['_: Main menu']]
add_time_EN = [['1.2 00:00'], ['1.2 00:30'], ['1.2 01:00'], ['1.2 01:30'], ['1.2 02:00'], ['1.2 02:30'], ['1.2 03:00'], ['1.2 03:30'], ['1.2 04:00'], ['1.2 04:30'], ['1.2 05:00'], ['1.2 05:30'], ['1.2 06:00'], ['1.2 06:30'], ['1.2 07:00'], ['1.2 07:30'], ['1.2 08:00'], ['1.2 08:30'], ['1.2 09:00'], ['1.2 09:30'], ['1.2 10:00'], ['1.2 10:30'], ['1.2 11:00'], ['1.2 12:00'], ['_. Main menu']]
add_time_XO = [['1:2 00:00'], ['1:2 00:30'], ['1:2 01:00'], ['1:2 01:30'], ['1:2 02:00'], ['1:2 02:30'], ['1:2 03:00'], ['1:2 03:30'], ['1:2 04:00'], ['1:2 04:30'], ['1:2 05:00'], ['1:2 05:30'], ['1:2 06:00'], ['1:2 06:30'], ['1:2 07:00'], ['1:2 07:30'], ['1:2 08:00'], ['1:2 08:30'], ['1:2 09:00'], ['1:2 09:30'], ['1:2 10:00'], ['1:2 10:30'], ['1:2 11:00'], ['1:2 12:00'], ['_: Main menu']]
add_start_EN = [['1.3 WILLOWALE'], ['1.3 NTLANGULA'], ['1.3 NGWANE'], ['1.3 MTOKWANE'], ['1.3 NONDOBO'], ['1.3 KUMKHULU'], ['1.3 NTUBENI'], ['1.3 MAYIJI'], ['1.3 NPUME'], ['1.3 MEVANA'],['_. Main menu']]
add_start_XO = [['1:3 WILLOWALE'], ['1:3 NTLANGULA'], ['1:3 NGWANE'], ['1:3 MTOKWANE'], ['1:3 NONDOBO'], ['1:3 KUMKHULU'], ['1:3 NTUBENI'], ['1:3 MAYIJI'], ['1:3 NPUME'], ['1:3 MEVANA'],['_: Main menu']]
add_arrival_EN = [['1.4 WILLOWALE'], ['1.4 NTLANGULA'], ['1.4 NGWANE'], ['1.4 MTOKWANE'], ['1.4 NONDOBO'], ['1.4 KUMKHULU'], ['1.4 NTUBENI'], ['1.4 MAYIJI'], ['1.4 NPUME'], ['1.4 MEVANA'],['_. Main menu']]
add_arrival_XO = [['1:4 WILLOWALE'], ['1:4 NTLANGULA'], ['1:4 NGWANE'], ['1:4 MTOKWANE'], ['1:4 NONDOBO'], ['1:4 KUMKHULU'], ['1:4 NTUBENI'], ['1:4 MAYIJI'], ['1:4 NPUME'], ['1:4 MEVANA'],['_: Main menu']]
dest_EN = [['2.1 WILLOWALE'], ['2.1 NTLANGULA'], ['2.1 NGWANE'], ['2.1 MTOKWANE'], ['2.1 NONDOBO'], ['2.1 KUMKHULU'], ['2.1 NTUBENI'], ['2.1 MAYIJI'], ['2.1 NPUME'], ['2.1 MEVANA'],['_. Main menu']]

file_offering = "dati/offering.csv"
file_reciving = "dati/reciving.csv"



def handler(msg):
    chat_id = msg['chat']['id']
    chat_message = msg['text']  if "text" in msg else "          "
    text_location = msg["location"] if "location" in msg else None 
    
    tmp_offering = "tmp/" + str(chat_id)
    tmp_choise = "tmp/" + str(chat_id) + "-c"
    
    print(chat_message)
    if text_location != None:
        if text_location['latitude'] > 35:
            bot.sendMessage(chat_id, 'Sorry! No passages available ', reply_markup={'keyboard': main_keyboard_EN})
        else:
            bot.sendMessage(chat_id, 'Insert the destination ', reply_markup={'keyboard': dest_EN})
    
    elif chat_message[0]=='_':
        if chat_message[2] != '2':
            bot.sendMessage(chat_id, 'Select option', reply_markup={'keyboard': main_keyboard_EN})
        else:
            bot.sendMessage(chat_id, 'Khetha ukhetho', reply_markup={'keyboard': main_keyboard_XO})

    elif chat_message=='start' or chat_message=='/start':
        bot.sendMessage(chat_id, 'Select language / Khetha ulwimi', reply_markup={'keyboard': [['0.1 English'], ['0.2 Xhosa']]})
    elif chat_message[0] == '0':
        #scelta della lingua
        if chat_message[2] != '2':
            bot.sendMessage(chat_id, 'Select option', reply_markup={'keyboard': main_keyboard_EN})
        else:
            bot.sendMessage(chat_id, 'Khetha ukhetho', reply_markup={'keyboard': main_keyboard_XO})


    elif chat_message[0] == '1':
        #sottomenu
        #keyboard
        
        #select date
        if chat_message[2] == ' ':
            if chat_message[1] == '.':
                bot.sendMessage(chat_id, 'Cool. Please select a date of departure ', reply_markup={'keyboard': add_date_EN})
            else:
                bot.sendMessage(chat_id, 'Cool. Nceda khetha omnye umhla lokuhamba ', reply_markup={'keyboard': add_date_XO})
            
            #first iteration / create file
            out_file = open(tmp_offering,"w")
            out_file.write(str(msg['from']['username'] + ','))
            out_file.close()

        #select time
        elif chat_message[2] == '1':
            if chat_message[1] == '.':
                bot.sendMessage(chat_id, 'Good. Now set the departure time ', reply_markup={'keyboard': add_time_EN})
            else:
                bot.sendMessage(chat_id, 'Good. Ngoku Ukuseta ixesha lokuhamba ', reply_markup={'keyboard': add_time_XO})
            #store tmp
            out_file = open(tmp_offering,"a")
            out_file.write(str(chat_message[4:]) + ',')
            out_file.close()
#start position
        #select start
        elif chat_message[2] == '2':
            if chat_message[1] == '.':
                bot.sendMessage(chat_id, 'Ok! Select the city ', reply_markup={'keyboard': add_start_EN})
            else:
                bot.sendMessage(chat_id, 'Ok! Khetha umzi ', reply_markup={'keyboard': add_start_XO})
            out_file = open(tmp_offering,"a")
            out_file.write(str(chat_message[4:]) + ',')
            out_file.close()

#start destination
        elif chat_message[2] == '3':
            if chat_message[1] == '.':
                bot.sendMessage(chat_id, 'Good! What\'s your destination? ', reply_markup={'keyboard': add_arrival_EN})
            else:
                bot.sendMessage(chat_id, 'ezilungileyo! Yintoni oya kuyo? ', reply_markup={'keyboard': add_arrival_XO})
            out_file = open(tmp_offering,"a")
            out_file.write(str(chat_message[4:]) + ',')
            out_file.close()

        elif chat_message[2] == '4':
            if chat_message[1] == '.':
                bot.sendMessage(chat_id, 'Congrats! Your journey has been added ', reply_markup={'keyboard': main_keyboard_EN})
            else:
                bot.sendMessage(chat_id, 'Congrats! Uhambo lwakho longeziwe. ', reply_markup={'keyboard': main_keyboard_XO})
            out_file = open(tmp_offering,"a")
            out_file.write(str(chat_message[4:]))
            out_file.close()
            #read tmp data
            in_file = open(tmp_offering,"r")
            in_line = in_file.readline()
            in_file.close()
            #store data
            out_file = open(file_offering,"a")
            out_file.write(in_line + "\n")
            out_file.close()

        #select arrival


        #_Main menu
        #if chat_message[4] == '.':


    elif chat_message[0] == '2' and chat_message[2] == ' ':
        if chat_message[1] == '.':
            bot.sendMessage(chat_id, 'Good! send us your location \n1) Push the icon (icon attachment) at the bottom\n2) Click on \"Position\" and then on \"Send location\"', reply_markup={'hide_keyboard': True})
        else:
            bot.sendMessage(chat_id, 'Kulungile! nisithumelele indawo yakho \ N1) Lityhalele icon (loncamathiselo icon) ezantsi \ N2) Cofa \ "Position \" kwaye ngoko \ "Thumela indawo \"', reply_markup={'hide_keyboard': True})
    elif chat_message[0] == '2':
            #print(chat_message[4:])
        #cerco destinazioni

        text = "Here are some results:\n"
        flag = 0
        in_file = open(file_offering,"r")
        while 1:
            in_line = in_file.readline()
            if in_line == "":
                break
            in_line = in_line[:-1]
            [user,data,time,start,end] = string.split(in_line,",")
            if end == chat_message[4:]:
                text += 'User: @' + user + ' leaving on ' + data + ' at ' + time + '\n'
                flag = 1
        in_file.close()

        if flag != 1:
            bot.sendMessage(chat_id, 'Sorry! No passages available ', reply_markup={'keyboard': main_keyboard_EN})
        else:
            bot.sendMessage(chat_id, text, reply_markup={'keyboard': main_keyboard_EN})
        

    elif chat_message[0] == '3':
        bot.sendMessage(chat_id, 'Working progress')

    elif chat_message[0] == '4':

        if chat_message[1] == '.':
            bot.sendMessage(chat_id, 'Select language', reply_markup={'keyboard': [['0.1 English'], ['0.2 Xhosa']]})
        else:
            bot.sendMessage(chat_id, 'Khetha ulwimi', reply_markup={'keyboard': [['0.1 English'], ['0.2 Xhosa']]})

    else:
        bot.sendMessage(chat_id, 'Invalid Message. please select an option from keyboard')

bot = telepot.Bot('244440158:AAEiXJDlpFYtE6mRceDpMrUACTuMgzLkxIo')
print('[+] started...')
bot.message_loop(handler, run_forever=True)