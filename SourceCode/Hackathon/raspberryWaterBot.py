
import time
import random
import datetime
import telepot
import os
import string
import math

maxDistance = 15
isAdding = False

def isAddingToTrue():
    global isAdding    # Needed to modify global copy of globvar
    isAdding = True
def isAddingToFalse():
    global isAdding    # Needed to modify global copy of globvar
    isAdding = False


def compassDirection (lat1, long1, lat2, long2) :
    margin = math.pi/90 
    o = lat1 - lat2
    a = long1 - long2
    angle = math.atan2(o,a)

    if angle > -margin and angle < margin :
        return "E"
    elif angle > math.pi/2 - margin and angle < math.pi/2 + margin :
        return "N"
    elif  angle > math.pi - margin and angle < -math.pi + margin :
        return "W"
    elif  angle > -math.pi/2 - margin and angle < -math.pi/2 + margin :
        return "S"
    
    if  angle > 0 and angle < math.pi/2 :
        return "NE"
    elif  angle > math.pi/2 and angle < math.pi :
        return "NW"
    elif  angle > -math.pi/2 and angle < 0 :
        return "SE"
    else :
        return "SW"
    
def distance_on_unit_sphere (lat1, long1, lat2, long2):
     
    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
     
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
     
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
     
    # Compute spherical distance from spherical coordinates.
     
    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta', phi')
    # cosine( arc length ) =
    # sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
     
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
    math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
     
    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc

def handle(msg):
    chat_id = msg['chat']['id']

    show_keyboard = {'keyboard': [['SPRINGS NEARBY'],['ADD SPRING']]}
    rating_keyboard = [['\xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90  \xE2\xAD\x90 \xE2\xAD\x90 ']]

    command = msg['text']  if "text" in msg else None 
    text_location = msg["location"] if "location" in msg else None 
    image = msg["photo"] if "photo" in msg else None 

    if image!=None:
        bot.sendMessage(chat_id,"Great. Now send me the spring's location.")
        isAddingToTrue()


    if command!=None:
        if command=='/start':
            bot.sendMessage(chat_id,"Great! Choose the option you prefer:",reply_markup=show_keyboard)

        elif command == 'ADD SPRING':
            bot.sendMessage(chat_id, 'Cool! Please take a picture of the spring.', reply_markup={'hide_keyboard': True})
            #update the pounds file with a new spring

        elif command == 'SPRINGS NEARBY':
            isAddingToFalse()
            bot.sendMessage(chat_id, 'Good. Send me your location.', reply_markup={'hide_keyboard': True})

        elif command == 'Back to Main menu':
            bot.sendMessage(chat_id, 'Great! Choose the option you prefer:', reply_markup=show_keyboard)

        elif "waterpond" in command :
            bot.sendChatAction(chat_id,'upload_photo')
            response = bot.sendPhoto(chat_id,open('foto/snap.jpg', 'rb')) 
            bot.sendMessage(chat_id, 'Great! Choose the option you prefer:', reply_markup=show_keyboard)

        else :
            bot.sendMessage(chat_id, 'Congrats! The spring has been added to the list!', reply_markup=show_keyboard)


    if text_location!=None and isAdding == False:
        file_pozzi = "dati/pozzi.csv"
        in_file = open(file_pozzi,"r")
        message = []
        while 1:
            in_line = in_file.readline()
            if in_line == "":
                break
            in_line = in_line[:-1]
            [name,latitude,longitude,water,quality] = string.split(in_line,",")
            rating = ' '
            if int(quality) == 1 :
                rating = '\xE2\xAD\x90'
            if int(quality) == 2 :
                rating = '\xE2\xAD\x90 \xE2\xAD\x90'
            if int(quality) == 3 :
                rating = '\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90'
            if int(quality) == 4 :
                rating = '\xE2\xAD\x90 \xE2\xAD\x90  \xE2\xAD\x90 \xE2\xAD\x90'
            if int(quality) == 5 :
                rating = '\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90'

            distanceFromPound = distance_on_unit_sphere(text_location['latitude'], text_location['longitude'], float(latitude), float(longitude)) * 6373
            if  distanceFromPound < maxDistance :
                message.append(name + ' ' + str(round(distanceFromPound/5*60,0)) + ' min' + ' ' + water + '% ' + compassDirection(text_location['latitude'], text_location['longitude'], float(latitude), float(longitude)) + rating)
        in_file.close()
        if message!=[]:
            bot.sendMessage(chat_id,"Click on the spring you want to view the queue of",reply_markup={'keyboard': [message]})
        else :
            bot.sendMessage(chat_id, 'No water pounds near!')

    if text_location!=None and isAdding == True:
        isAddingToFalse()
        sub_buttons = {'keyboard': rating_keyboard}
        bot.sendMessage(chat_id, "Good. What's the quality of the water?", reply_markup=sub_buttons)


bot = telepot.Bot('277723656:AAHzwF_Kw12bGtHNzuRP266xpjqIT8zulU8')
#bot.notifyOnMessage(handle)
print('[+] started...')
bot.message_loop(handle, run_forever=True)

#while 1:
#	time.sleep(10)


