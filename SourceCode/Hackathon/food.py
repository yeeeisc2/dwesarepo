
import time
import random
import datetime
import telepot
import os
import string
import math

maxDistance = 15
isAdding = False

def isAddingToTrue():
    global isAdding    # Needed to modify global copy of globvar
    isAdding = True
def isAddingToFalse():
    global isAdding    # Needed to modify global copy of globvar
    isAdding = False


def compassDirection (lat1, long1, lat2, long2) :
    margin = math.pi/90 
    o = lat1 - lat2
    a = long1 - long2
    angle = math.atan2(o,a)

    if angle > -margin and angle < margin :
        return "E"
    elif angle > math.pi/2 - margin and angle < math.pi/2 + margin :
        return "N"
    elif  angle > math.pi - margin and angle < -math.pi + margin :
        return "W"
    elif  angle > -math.pi/2 - margin and angle < -math.pi/2 + margin :
        return "S"
    
    if  angle > 0 and angle < math.pi/2 :
        return "NE"
    elif  angle > math.pi/2 and angle < math.pi :
        return "NW"
    elif  angle > -math.pi/2 and angle < 0 :
        return "SE"
    else :
        return "SW"
    
def distance_on_unit_sphere (lat1, long1, lat2, long2):
     
    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
     
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
     
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
     
    # Compute spherical distance from spherical coordinates.
     
    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta', phi')
    # cosine( arc length ) =
    # sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
     
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
    math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
     
    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc

def handle(msg):
    chat_id = msg['chat']['id']

    show_keyboard = {'keyboard': [['FOOD NEARBY'],['SHARE FOOD']]}
    rating_keyboard = [['\xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90'], ['\xE2\xAD\x90 \xE2\xAD\x90 \xE2\xAD\x90  \xE2\xAD\x90 \xE2\xAD\x90 ']]

    command = msg['text']  if "text" in msg else None 
    text_location = msg["location"] if "location" in msg else None 
    image = msg["photo"] if "photo" in msg else None 

    if image!=None:
        bot.sendMessage(chat_id,"Thanks for sending your picture. Now submit your location.")
        isAddingToTrue()


    if command!=None:
        if command=='/start':
            bot.sendMessage(chat_id,"Great! Choose the option you prefer:",reply_markup=show_keyboard)

        elif command == 'SHARE FOOD':
            bot.sendMessage(chat_id, 'Cool! Please add a description of the food you are offering.', reply_markup={'hide_keyboard': True})
            #update the pounds file with a new spring

        elif command == 'FOOD NEARBY':
            isAddingToFalse()
            bot.sendMessage(chat_id, 'Good. Send me your location.', reply_markup={'hide_keyboard': True})

        elif command == 'Back to Main menu':
            bot.sendMessage(chat_id, 'Great! Choose the option you prefer:', reply_markup=show_keyboard)

        elif "food" in command :
            bot.sendChatAction(chat_id,'upload_photo')
            response = bot.sendPhoto(chat_id,open('foto/food.jpg', 'rb'))
            bot.sendMessage(chat_id, '@Username', reply_markup={'hide_keyboard': True})
            bot.sendMessage(chat_id, 'Great! Choose the option you prefer:', reply_markup=show_keyboard)

        else :
            bot.sendMessage(chat_id, 'Please take a picture of the meal.', reply_markup=show_keyboard)


    if text_location!=None and isAdding == False:
        food_file = "dati/food.csv"
        in_file = open(food_file,"r")
        message = []
        while 1:
            in_line = in_file.readline()
            if in_line == "":
                break
            #in_line = in_line[:-1]
            [name,latitude,longitude,foodDescription] = string.split(in_line,",")
            

            distanceFromPound = distance_on_unit_sphere(text_location['latitude'], text_location['longitude'], float(latitude), float(longitude)) * 6373
            if  distanceFromPound < maxDistance :
                message.append(name + ' ' + str(round(distanceFromPound/5*60,0)) + ' min' + ' ' + foodDescription + ' ' + compassDirection(text_location['latitude'], text_location['longitude'], float(latitude), float(longitude)) )
        in_file.close()
        if message!=[]:
            bot.sendMessage(chat_id,"Click on the food you want to request.",reply_markup={'keyboard': [message]})
        else :
            bot.sendMessage(chat_id, 'No food near!')

    if text_location!=None and isAdding == True:
        isAddingToFalse()
        bot.sendMessage(chat_id, 'Congrats! Your food has been added!', reply_markup=show_keyboard)


bot = telepot.Bot('289433033:AAGZlg8j0PLe-LOEn4GQdldGbOmXr7Zmn8M')
#bot.notifyOnMessage(handle)
print('[+] started...')
bot.message_loop(handle, run_forever=True)

#while 1:
#	time.sleep(10)


