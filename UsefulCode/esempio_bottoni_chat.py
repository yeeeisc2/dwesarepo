import sys
import time
import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from tinydb import TinyDB, Query

def on_chat_message(msg):

    content_type, chat_type, chat_id = telepot.glance(msg)

    keyboard = InlineKeyboardMarkup(inline_keyboard=[
                   [InlineKeyboardButton(text='Load more', callback_data='press')],
               ])
    messageToEdit = bot.sendMessage(chat_id, 'Press on the Load more button to view results', reply_markup=keyboard)
    seenDB.insert_multiple([{'id': chat_id, 'seenPosts' : 0, 'msgEdit' : messageToEdit}])

def on_callback_query(msg):
	
    keyboard = InlineKeyboardMarkup(inline_keyboard=[
                   [InlineKeyboardButton(text='Load more', callback_data='press')],
               ])
               

    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
    print('Callback Query:', query_id, from_id, query_data)

    if seenDB.search(Query()['id'] == from_id and Query()['seenPosts'] != 0) :
      startFrom = seenDB.search(Query()['id'] == from_id)[0]['seenPosts']
      seenDB.update({'seenPosts': startFrom + 3}, Query()['id'] == from_id)
    else :
      seenDB.insert_multiple([{'id': from_id, 'seenPosts' : 4}])
      startFrom = 1

    messageToEdit = seenDB.search(Query()['id'] == from_id)[0]['msgEdit']
    botAnswer = ''
    isEmpty = True
    for i in range(startFrom, startFrom + 3):
      queryObj = testDB.get(eid=i)
      if queryObj!=None :
        botAnswer += str(queryObj)
        isEmpty = False
    if not isEmpty :
    	bot.editMessageText(telepot.message_identifier(messageToEdit),botAnswer, reply_markup=keyboard)
    else : 
        try :
            bot.editMessageText(telepot.message_identifier(messageToEdit),'No more results please try again later!', reply_markup=keyboard)
        except :
            bot.answerCallbackQuery(query_id, text='Still no new results')
    		


testDB = TinyDB('testDB.json')
seenDB = TinyDB('seenDB.json')
#this following 2 lines are to be deleted
seenDB.purge()
testDB.purge()
# .......
testDB.insert_multiple([{'fruit': 'mela' }, {'fruit': 'arancia' }, {'fruit': 'susina' }, {'fruit': 'fragola' }, {'fruit': 'diocane' }, {'fruit': 'limone' }, {'fruit': 'noce' }, {'fruit': 'culo' }, {'fruit': 'merda' }, {'fruit': 'ualla' }]) 

bot = telepot.Bot('244440158:AAEiXJDlpFYtE6mRceDpMrUACTuMgzLkxIo')
bot.message_loop({'chat': on_chat_message,
                  'callback_query': on_callback_query})
print('Listening ...')

while 1:
    time.sleep(10)