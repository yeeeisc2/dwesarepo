import json

#read the file
jsonFile = open('../SourceCode/json/languages.json', 'r')
#store the file in json variable
languages = json.load(jsonFile)
jsonFile.close()

#selected language
__language = 'EN'
#put the following variables in the correct file
__error_reading_languages = {'EN':'ERROR reading messages in different languages','IT':'ERRORE durante la lettura dei messaggi in lingue differenti','XO':''}

#put the following variables in the correct file
__str_username = '#nomeutente'



#function that read the message in differente languages
def select_message(language_id):
	#check if 'languages' exist in languages
	if 'languages' in languages:
		#check if language_id exist in languages['languages']
		if language_id in languages['languages']:
			if __language in languages['languages'][language_id]:
				#all correct return
				return languages['languages'][language_id][__language]
			print 'translation not found'
		print 'id not found'
	print __error_reading_languages[__language]
	#error return
	return __error_reading_languages[__language]

#take the message
message = select_message('2102')

#modify the message
print message.replace(__str_username, '@magikone')
print select_message('2102').replace(__str_username, '@magikone')

