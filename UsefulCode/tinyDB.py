from tinydb import TinyDB, Query
import json
import ujson


db = TinyDB('db.json', ensure_ascii = False)

insert = {'name': 'silvio', 'age' : 22}
db.insert_multiple([insert])

insert = {'name': 'luca', 'age' : 21}
db.insert_multiple([insert])

Person = Query()
data = db.search((Person['name'] == 'silvio') | (Person['name'] == 'luca'))

print json.dumps(db.all(), indent=2)
print db.all()[0]['name']

#print db.all()[0]



print
print Query()



db.purge()
